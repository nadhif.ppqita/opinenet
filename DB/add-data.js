const { sql } = require("@vercel/postgres");
require('dotenv').config({path: '.env.development.local'});

async function execute() {
    const name = 'Muhammad Nadhif';
    const website = 'contohlinkwebanda.com';
    const comment = 'Lanjutkan karyamu!';

    const addData = await sql`
    insert into comment(name, website, comment) values(${name}, ${website}, ${comment})
    `;
    console.log(addData);
}

execute();