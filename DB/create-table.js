const { sql } = require("@vercel/postgres");
require('dotenv').config({path: ".env.development.local"});

async function execute() {
    const deleteTable = await sql`drop table if exists comment`;
    const createTable = await sql`create table if not exists comment(
    id serial primary key,
    name varchar(20) not null,
    website varchar(50),
    comment varchar(160) not null
    )`

    console.log(createTable);
}

execute();