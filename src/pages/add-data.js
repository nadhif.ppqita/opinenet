import { useRouter } from "next/router";
import { useState } from "react";

export default function Home() {
    const [inputValueName, setInputValueName] = useState('');
    const [inputValueWebsite, setInputValueWebsite] = useState('');
    const [inputValueComment, setInputValueComment] = useState('');
    const router = useRouter();

    const handleSubmit = (event) => {
        event.preventDefault();
        fetch('/api/addData', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: inputValueName,
                website: inputValueWebsite,
                comment: inputValueComment,
            })
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Gagal menambah data.');
                }
                return response.json();

            })
            .then((json) => {
                console.log(json);
                alert('Data berhasil ditambah');
                router.push('/')
            })
            .catch((error) => {
                console.error('Error saat menambah data', error.message);
                alert('Error saat menambah data: ' + error.message);
            })
    }

    const handleChangeName = (event) => {
        setInputValueName(event.target.value);
    }

    const handleChangeWebsite = (event) => {
        setInputValueWebsite(event.target.value);
    }

    const handleChangeComment = (event) => {
        setInputValueComment(event.target.value);
    }

    return (
        <div>
            <h1 className="text-3xl p-5 text-center">Add Comment</h1>
            <div className="border border-black rounded-lg mx-20 my-10 p-5">
                <form onSubmit={handleSubmit}>
                    <div className="mb-4">
                        <label htmlFor="name" className="block text-sm font-medium text-black">Name:</label>
                        <input type="text" name="name" id="name" onChange={handleChangeName} className="mt-1 p-2 border border-black rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm" />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="website" className="block text-sm font-medium text-black">Website:</label>
                        <input type="text" name="website" id="website" onChange={handleChangeWebsite} className="mt-1 p-2 border border-black rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm" />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="comment" className="block text-sm font-medium text-black">Comment:</label>
                        <input type="text" name="comment" id="comment" onChange={handleChangeComment} className="mt-1 p-2 border border-black rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm" />
                    </div>
                    <button type="submit" className="inline-flex items-center px-4 py-2 border border-black rounded-md shadow-sm text-sm font-medium text-black bg-white hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-black">Add Data</button>
                </form>
            </div>
        </div>
    )
}
