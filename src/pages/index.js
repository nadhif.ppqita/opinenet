import { useState, useEffect } from "react";
import { useRouter } from 'next/router';

export default function Home() {
  const [showData, setShowData] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const router = useRouter();

  useEffect(() => {
    fetch('/api/getData')
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setShowData(data);
        setLoading(false);
      });
  }, []);

  if (isLoading) return <p>Loading...</p>;
  if (!showData || showData.length === 0) return <p>No commentaries data</p>;
  return (
    <div>
      <h1 className="text-3xl md:pl-5 xl:pl-10 p-5 pb-0 sm:pl-10 pl-12">OpineNet.</h1>
      <p className="xl:pl-10 xl:mt-1 md:pl-6 sm:pl-10 pl-12">Sebelum Komen, Kalian Cek Karya Saya!</p>
      <div>
      <a className="xl:pl-10 md:pl-6 sm:pl-10 pl-12" href="https://medium.com/@nadhif.ppqita">My Medium</a>
      <a className="xl:pl-10 md:pl-6 sm:pl-10 pl-12" href="https://github.com/MNadhifAttamimi">My Github</a>
      </div>
      
      <div className="xl:space-y-8 md:space-y-4 sm:space-y-2 xl:mx-auto md:mx-auto sm:m-8 m-10 max-w-3xl xl:overflow-y-auto md:overflow-y-auto" style={{ maxHeight: "calc(100vh - 200px)" }}>
      {showData && showData.map((data, index) => (
  <div className="border border-black rounded-lg p-4 mb-4" key={index}>
    <p className="font-semibold">ID: {data.id}</p>
    <p>Name: {data.name}</p>
    <p>Website: {data.website}</p>
    <p>Comment: {data.comment}</p>
  </div>
))}
<button onClick={() => router.push(`/add-data`)} className=" mb-10 border border-black px-4 py-2 rounded-md mt-4 xl:hidden md:hidden">Add Comment</button>
      </div>
      <div className="xl:flex justify-end md:flex justify-end sm:hidden hidden ">
        <button onClick={() => router.push(`/add-data`)} className="mb-10 border border-black px-4 py-2 rounded-md mr-12">Add Comment</button>
      </div>
    </div>
  );
}
