const { sql } = require("@vercel/postgres");

async function addData(req, res) {
    try {
        if (req.method !== "POST") {
            return res.status(405).json({ message: "Method Tidak Sama" });
        }

        const { name, website, comment } = req.body;

        if (!name) {
            return res.status(400).json({ message: "nama harus diisi" });
        }
        if (!comment) {
            return res.status(400).json({ message: "comment harus diisi" });
        }

        const addData = await sql`
            INSERT INTO comment (name, website, comment) VALUES (${name}, ${website}, ${comment})
        `;
        res.status(200).json({ message: "Success to Insert Data", data: addData });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "Terjadi Error" });
    }
}

export default addData;
