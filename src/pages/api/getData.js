const { sql } = require("@vercel/postgres");

export default async function getData(req, res) {
    try {
        if (req.method !== "GET") {
            return res.status(405).json({message: 'Method Tidak Sama'})
        }
        const result = await sql`select * from comment`;
        res.status(200).json(result.rows)
    } catch (error) {
        console.log(error);
        return res.status(500).json({message: "Terjadi Error"})
    }
}